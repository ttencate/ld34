/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>

const BULLET_SPEED = 1024;
const BULLET_LIFETIME = 700;
const BULLET_DAMAGE = 20;

function createBullet(game: Phaser.Game, x: number, y: number, degrees: number, vx: number, vy: number, playerOwned: boolean) {
  var bullet = game.world.getFirstDead(true, x, y, playerOwned ? 'bullet' : 'enemy_bullet');
  game.physics.p2.enable(bullet);
  // bullet.body.kinematic = true;
  bullet.body.setZeroDamping();
  bullet.body.mass = 0.05;

  var velocity = new Phaser.Point();
  velocity.set(0, -BULLET_SPEED);
  velocity.rotate(0, 0, degrees, true);
  velocity.add(vx, vy);
  bullet.body.velocity.x = velocity.x;
  bullet.body.velocity.y = velocity.y;
  

  if (playerOwned) {
    bullet.body.setCollisionGroup(game.cgroups.playerbullets);
    bullet.body.collides([game.cgroups.tilemap, game.cgroups.components, game.cgroups.enemyship]);
    bullet.body.createGroupCallback(game.cgroups.enemyship, bulletHitComponent);
  } else {
    bullet.body.setCollisionGroup(game.cgroups.enemybullets);
    bullet.body.collides([game.cgroups.tilemap, game.cgroups.components, game.cgroups.playership]);
    bullet.body.createGroupCallback(game.cgroups.playership, bulletHitComponent);
  }
  bullet.body.createGroupCallback(game.cgroups.tilemap, bulletHitTilemap);
  bullet.body.createGroupCallback(game.cgroups.components, bulletHitComponent);

  game.time.events.add(BULLET_LIFETIME, function() {
    bullet.pendingDestroy = true;
  });
}

function bulletHitTilemap(bulletBody: Phaser.Physics.P2.Body) {
  bulletBody.sprite.pendingDestroy = true;
}

function bulletHitComponent(bulletBody: Phaser.Physics.P2.Body, componentBody: Phaser.Physics.P2.Body) {
  if (!bulletBody || !componentBody) return;
  var bullet = bulletBody.sprite;
  var component = componentBody.sprite;
  if (component instanceof Component) {
    bullet.pendingDestroy = true;
    doDamage(component);
  } else if ((<any>component).owner) {
    // It's a shield. Hopefully.
    doDamage((<any>component).owner);
  }
}

function doDamage(component: Component) {
  component.health -= BULLET_DAMAGE;
  if (component.health <= 0) {
    component.health = 0;
    component.explode();
    window.game.score.addPoints(10 * window.game.multiplyer);
  }
}
