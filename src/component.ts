/// <reference path="../lib/phaser.d.ts"/>

class Component extends Phaser.Sprite {

  // These match 1:1 by their array index.
  private attached: Array<Component> = [];
  private constraints: Array<Phaser.Physics.P2.LockConstraint> = [];

  protected owner: Component; // 'parent' is already taken
  protected playerOwned: boolean;

  constructor(game: Phaser.Game, x: number, y: number, spriteName: string) {
    super(game, x, y, spriteName);

    game.physics.p2.enable(this);
    this.body.collides([
      this.game.cgroups.tilemap,
      this.game.cgroups.components,
      this.game.cgroups.playership,
      this.game.cgroups.enemyship,
      this.game.cgroups.playerbullets,
      this.game.cgroups.enemybullets
    ]);
    this.playerOwned = false;
    this.setPartOfShip(false);

    this.maxHealth = 100;
    this.health = 100;

    // Make mass depend on sprite surface area (with some sensible scale factor).
    this.body.mass = this.width * this.height / 4096;
  }

  // Call this after you've messed with the shapes in the body.
  protected fixCollisionGroup() {
    if (this.owner || this instanceof BaseShip) {
      if (this.playerOwned || this instanceof Ship) {
        this.body.setCollisionGroup(this.game.cgroups.playership);
      } else {
        this.body.setCollisionGroup(this.game.cgroups.enemyship);
      }
    } else {
      this.body.setCollisionGroup(this.game.cgroups.components);
    }
  }

  update() {
    var factor = this.health / this.maxHealth;
    var pht = this.preHealthTint();
    this.tint =
      ((factor * (pht >> 16)) << 16) +
      ((factor * ((pht >> 8) & 0xff)) << 8) +
      (factor * (pht & 0xff));

    this.restoreHealth();
    var speed = Math.sqrt(this.body.velocity.x * this.body.velocity.x + this.body.velocity.y * this.body.velocity.y);
    var maxSpeed = 400;
    if (speed > maxSpeed) {
      var force = new Phaser.Point(-this.body.velocity.x, -this.body.velocity.y);
      //force.rotate(0, 0, this.body.rotation, false);
      this.body.force.x += force.x;
      this.body.force.y += force.y;
    }
  }

  protected preHealthTint(): number {
    return 0xffffff;
  }

  restoreHealth() {
    this.health += 0.5;
    if (this.health > this.maxHealth) this.health = this.maxHealth;
  }

  private hitShip(myBody: Phaser.Physics.P2.Body, shipBody: Phaser.Physics.P2.Body) {
    if (shipBody) {
      var ship = shipBody.sprite;
      if (ship instanceof Component) {
        var that = ship;
        if (this instanceof Warhead && that.playerOwned) {
              for (var i = 0; i < 30; i++) {
                createBullet(this.game, this.x, this.y, Math.random() * 360, this.body.velocity.x, this.body.velocity.y, false);
              }
              for (var i = 0; i < 5; i++) {
                createBullet(this.game, this.x, this.y, Math.random() * 360, this.body.velocity.x, this.body.velocity.y, true);
              }
        }
        if (this instanceof BaseShip && that.owner) { return; }
        if (that instanceof BaseShip && this.owner) { return; }
        if (this instanceof BaseShip && that instanceof BaseShip) { return; }
        if (this instanceof BaseMine && !that.playerOwned) { return; }

        if (this.owner) { return; }

        if (ship && ship instanceof Component && (ship instanceof BaseShip || ship.owner)) {
          this.setPartOfShip(true);
          ship.attach(this);
        }
      }
    }
  }

  protected setPartOfShip(partOfShip: boolean) {
    if (partOfShip) {
      if (this instanceof Ship || this.playerOwned) {
        this.body.setCollisionGroup(this.game.cgroups.playership);
        this.body.removeCollisionGroup(this.game.cgroups.playership);
      } else {
        this.body.setCollisionGroup(this.game.cgroups.enemyship);
        this.body.removeCollisionGroup(this.game.cgroups.enemyship);
      }
    } else {
      // Make it free-floating again.
      this.body.setCollisionGroup(this.game.cgroups.components);
      this.body.collides([this.game.cgroups.playership, this.game.cgroups.enemyship]);
      this.body.createGroupCallback(this.game.cgroups.playership, this.hitShip, this);
      this.body.createGroupCallback(this.game.cgroups.enemyship, this.hitShip, this);
    }
  }

  // Attaches the given component as a child to this one.
  attach(component: Component) {
    this.attached.push(component);
    component.owner = this;
    component.playerOwned = this.playerOwned;

    var p = new Phaser.Point(this.body.x - component.body.x, this.body.y - component.body.y);
    // Note: on Body, rotation is in radians, angle is in degrees!
    // Maybe. This doesn't work correctly if I try to do it in radians...
    p.rotate(0, 0, -this.body.angle, true);
    var angle = component.body.rotation - this.body.rotation;
    var constraint = this.game.physics.p2.createLockConstraint(this.body, component.body, [p.x, p.y], angle, 10000000000000000000000000);
    this.constraints.push(constraint);

    component.setPartOfShip(true);
    component.onAttach();
  }

  // Detaches the given child component.
  detach(component: Component) {
    var index = this.attached.indexOf(component);
    if (index < 0) return;
    
    component.onDetach();

    this.attached.splice(index, 1);
    this.game.physics.p2.removeConstraint(this.constraints[index]);
    this.constraints.splice(index, 1);

    component.owner = null;
    component.setPartOfShip(false);
  }

  detachChildrenRecursively() {
    while (this.attached.length > 0) {
      this.attached[0].detachChildrenRecursively();
      this.detach(this.attached[0]);
    }
  }

  onAttach() {
  }

  onDetach() {
  }

  // Calls f for each child that is a component of this ship, recursively.
  forEachComponent(f: (component: Component) => any) {
    f(this);
    for (var i = 0; i < this.attached.length; i++) {
      var child = this.attached[i];
      if (child instanceof Component) {
        child.forEachComponent(f);
      }
    }
  }
  
  countChildren(){
    var count:number = this.attached.length;
    for (var i = 0; i < this.attached.length; i++) {
      var child = this.attached[i];
      if (child instanceof Component) {
      count += child.countChildren();
      }
    }
    return count;
  }

  explode() {
    this.detachChildrenRecursively();
    if (this.owner) {
      this.owner.detach(this);
    }
    this.pendingDestroy = true;

    window.game.sfx.play('explosion1');

    window.game.explosion.emitX = this.x;
    window.game.explosion.emitY = this.y;
    window.game.explosion.explode(1500, 30);
  }
}
