/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>
/// <reference path="engine.ts"/>
/// <reference path="shield.ts"/>
/// <reference path="base_ship.ts"/>
/// <reference path="main.ts"/>


class Turret extends BaseShip {

  private pilot: Pilot;
  
  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'enemy-core');
    this.pilot = new Pilot(this);
  }
  
  constructShip(){
    var gun1 = new Gun(this.game, this.x+25, this.y-40);
    var gun2 = new Gun(this.game, this.x-25, this.y-40);
    var shield = new Shield(this.game, this.x, this.y+50);
    this.game.world.addChild(gun1);
    this.game.world.addChild(gun2);
    this.game.world.addChild(shield);
    this.attach(gun1);
    this.attach(gun2);
    this.attach(shield);
        
    this.maxHealth = 100;
    this.health = 100;
  }

  update() {
    super.update();

    // use any engines accidentally picked up
    this.pilot.rotateTowardsPlayer(0);
    this.pilot.accelerateToSpeed(100);

    if (this.pilot.distanceToPlayer() < 500) {
      this.pilot.rotateGunTowardsPlayer();
      this.pilot.fire();
    }
  }
  
  explode(){
    super.explode();
    var newShield = new Shield(this.game, this.x, this.y);
    this.game.world.addChild(newShield);
  }
}
