/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>

class Spawner extends Phaser.Sprite {

  private ctor: any;
  private remainingSpawns: number;

  constructor(game: Phaser.Game, x: number, y: number, rotation: number, properties: any) {
    super(game, x, y, 'mine-spawner');
    this.anchor.set(0.5, 0.5);

    var type = properties.type;
    this.ctor = (<any>window)[type];
    if (typeof this.ctor != 'function') {
      console.warn('No spawner object type named "' + type + '"');
      return;
    }

    var period = properties.period;
    var phase: number = properties.phase || Math.random() * period;
    this.game.time.events.add(phase, function(){
      this.game.time.events.loop(period, this.spawn, this);
    }, this);

    this.remainingSpawns = parseInt(properties.max, 10) || 1;
  }

  update() {
    this.angle += 2;
  }

  spawn() {
    if (this.remainingSpawns <= 0) {
      return;
    }
    this.remainingSpawns--;

    var spawn = new this.ctor(this.game, this.x, this.y, this.rotation);
    spawn.events.onDestroy.add(() => { this.remainingSpawns++; });
    this.game.world.addChild(spawn);
    var velocity = Math.random() * 200 + 200;
    var angle = Math.random() * 360;
    spawn.body.velocity.x = Math.cos(angle) * velocity;
    spawn.body.velocity.y = Math.sin(angle) * velocity;
  }
}
