/// <reference path="game.ts"/>

// approximation only
function tanh(x: number) {
  if( x < -3 )
    return -1;
  else if( x > 3 )
    return 1;
  else
    return x * ( 27 + x * x ) / ( 27 + 9 * x * x );
}

interface Window {
  game: SimpleGame;
  restartGame(): void;
}

window.restartGame = () => {
  if (window.game) {
    window.game.destroy();  
    window.game = null;
    var elm = document.getElementsByTagName('canvas').item(0)
    if (elm != null) elm.remove();
  }

  var level = document.location.hash.substring(1);
  window.game = new SimpleGame(level || 'arena');
}

window.onload = () => {
  window.restartGame();
};

