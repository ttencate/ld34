/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>
/// <reference path="engine.ts"/>

class BaseShip extends Component {

  private parts: Array<Component> = [];
  protected engineSound: Phaser.Sound;

  constructor(game: Phaser.Game, x: number, y: number, key: string) {
    super(game, x, y, key);
    this.setPartOfShip(true);

    this.body.setCircle((this.width + this.height) / 4, 0, 0, 0);
    this.fixCollisionGroup();
    this.engineSound = this.game.add.audioSprite('sfx').get('thrust_loop');
    this.constructShip();
  }

  // Includes the core, so always >= 1.
  getNumComponents(): number {
    return this.countChildren();
  }
  
  constructShip(){}

  // Must be called exactly once per frame.
  rotate(direction: number) {
    if (!this.body) return;
    this.body.rotateLeft(direction * 50 * Math.sqrt(1 + this.countChildren()));

  }

  // Must be called exactly once per frame.
  accelerate(direction: number): boolean {
    if (!this.body) return;
    var myAngle = this.body.angle;
    var thrusted = false;

    this.forEachComponent((part) => {
      if (part instanceof Engine) {
        if (direction == 0) {
          part.thrust(false);
        } else {
          var thrusterAngle = part.body.angle;
          var absDelta = Math.abs(((thrusterAngle - myAngle) % 360 + 180) % 360 - 180);
          if (absDelta > 90 && direction < 0) {
            part.thrust(true);
            thrusted = true;
          } else if (absDelta < 90 && direction > 0) {
            part.thrust(true);
            thrusted = true;
          }
        }
      }
    });

    return thrusted;
  }

  // Must be called exactly once per frame.
  fire(firing: boolean) {
    if (!this.body) return;

    this.forEachComponent((part) => {
      if (part instanceof Gun) {
        part.fire(firing);
      }
    });
  }
}
