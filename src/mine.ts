/// <reference path="component.ts"/>

const MINE_MASS = 0.1;
const MINE_EXPLODE_TIME = 4000;

class BaseMine extends Component {
  
  public shieldDrop:boolean;

  constructor(game: Phaser.Game, x: number, y: number, key: string) {
    super(game, x, y, key);
    // this.scale.setTo(0.5, 0.5);
    this.body.clearShapes();
    this.body.setCircle((this.width + this.height) / 4, 0, 0, 0);
    this.fixCollisionGroup();
    this.body.mass = MINE_MASS;
    this.maxHealth = 50;
    this.health = 50;
    this.shieldDrop = true;
  }

  private pht: number = 0xffffff;
  protected preHealthTint(): number {
    return this.pht;
  }
 
  onAttach() {
    this.shieldDrop = false;
    var blinkTimer = this.game.time.events.loop(200, function() {
      if (!this.game) return;
      if (this.pht == 0xffffff) {
        this.pht = 0xff0000;
      } else {
        this.pht = 0xffffff;
      }
    }, this);
    this.game.time.events.add(MINE_EXPLODE_TIME, function() {
      if (!this.game) return;
      this.game.time.events.remove(blinkTimer);
      for (var i = 0; i < 30; i++) {
        createBullet(this.game, this.x, this.y, Math.random() * 360, this.body.velocity.x, this.body.velocity.y, this.enemyOwned);
      }
      this.explode();
    }, this);
    // Allow the player to crash the mine into a wall
    this.body.createGroupCallback(this.game.cgroups.tilemap, function() { this.health = 0; this.explode(); }, this);

  }
  
  explode(){
    super.explode();
    if (this.shieldDrop && Math.random() > 0.7){
      var newShield = new Shield(this.game, this.x, this.y);
      this.game.world.addChild(newShield);
    }
  }
}

class Mine extends BaseMine {
  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'mine');
  }
}
