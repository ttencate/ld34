/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="collisiongroups.ts"/>
/// <reference path="engine.ts"/>
/// <reference path="gun.ts"/>
/// <reference path="sensor.ts"/>
/// <reference path="ship.ts"/>
/// <reference path="mine.ts"/>
/// <reference path="spawner.ts"/>

// All objects that need to be returned to the Game.
interface LevelObjects {
  ship: Ship;
  background: Background;
  layer: Phaser.TilemapLayer;
}

declare module Phaser {
  interface Game {
    cgroups: CollisionGroups;
  }
}

function setUpLevel(game: Phaser.Game, name: string): LevelObjects {
  game.physics.startSystem(Phaser.Physics.P2JS);
  game.physics.p2.setImpactEvents(true);
  game.cgroups = {
    tilemap: game.physics.p2.createCollisionGroup(),
    playership: game.physics.p2.createCollisionGroup(),
    enemyship: game.physics.p2.createCollisionGroup(),
    components: game.physics.p2.createCollisionGroup(),
    playerbullets: game.physics.p2.createCollisionGroup(),
    enemybullets: game.physics.p2.createCollisionGroup(),
  };

  game.stage.backgroundColor = '#2d2d2d';

  var map = game.add.tilemap(name);

  map.addTilesetImage('tileset');

  var layer = map.createLayer('main');
  if (!layer) throw new Error('tilemap must contain tile layer named "main"');
  layer.resizeWorld();
  // layer.debug = true; // enable to see collision bounds

  map.setCollisionBetween(0, 15);

  var tilemapBodies = game.physics.p2.convertTilemap(map, layer);
  for (var i = 0; i < tilemapBodies.length; i++) {
    var body = tilemapBodies[i];
    body.setCollisionGroup(game.cgroups.tilemap);
    body.collides([game.cgroups.playership, game.cgroups.enemyship, game.cgroups.components, game.cgroups.playerbullets, game.cgroups.enemybullets]);
  }
  game.physics.p2.setBoundsToWorld(true, true, true, true, false);

  var levelObjects: LevelObjects = {
    ship: null,
    background: new Background(game),
    layer: layer,
  };

  var objectLayer = (<any>map.objects)['objects'];
  if (!objectLayer) throw new Error('tilemap must contain object layer named "objects"');
  objectLayer.forEach(function(object: any) {
    var ctor: any = window[object.name];
    if (typeof ctor != 'function') {
      console.warn('No object type named "' + object.name + '"');
      return;
    }
    var obj = new ctor(game, object.x, object.y, object.rotation, object.properties);
    game.world.addChild(obj);
    if (obj instanceof Ship) {
      levelObjects.ship = obj;
    }
  });

  //var spawner = new SubspaceySpawner(game, 3000);

  return levelObjects;
}
