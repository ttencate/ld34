/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>
/// <reference path="engine.ts"/>
/// <reference path="base_ship.ts"/>

class Ship extends BaseShip {

  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'ship');
  }
  
  constructShip(){
    this.playerOwned = true;

    var leftEngine = new Engine(this.game, this.x - 45, this.y + 20);
    var rightEngine = new Engine(this.game, this.x + 45, this.y + 20);
    this.game.world.addChild(leftEngine);
    this.game.world.addChild(rightEngine);
    this.attach(leftEngine);
    this.attach(rightEngine);
    this.maxHealth = 600;
    this.health = 600;
    this.body.mass = 4; 
  }

  accelerate(direction: number): boolean {
    var thrusted = super.accelerate(direction);

    if (thrusted && !this.engineSound.isPlaying) {
      this.engineSound.play('thrust_loop', null, 0.5, true);
    } else if (!thrusted && this.engineSound.isPlaying) {
      this.engineSound.stop();
    }

    return thrusted;
  }
  
  explode(){
    this.health = 0;
    super.explode();
    window.game.gameOver();
  }
}
