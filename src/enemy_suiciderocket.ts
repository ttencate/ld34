/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>
/// <reference path="engine.ts"/>
/// <reference path="base_ship.ts"/>
/// <reference path="mine.ts"/>
/// <reference path="main.ts"/>
/// <reference path="sensor.ts"/>

const LUDICROUS = 1000000;

class SuicideRocket extends BaseShip {
  
  private pilot: Pilot;
  
  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'enemy-core');
    this.pilot = new Pilot(this);
    this.maxHealth = 50;
    this.health = 50;

  }
  
  constructShip(){
    var singleEngine = new Engine(this.game, this.x, this.y + 55);
    var rammingShield = new Warhead(this.game, this.x, this.y - 42);
    this.game.world.addChild(singleEngine);
    this.game.world.addChild(rammingShield);
    this.attach(singleEngine);
    this.attach(rammingShield);
  }

  update() {
    super.update();
    this.pilot.rotateTowardsPlayer(10);
    var distanceToPlayer = this.pilot.distanceToPlayer();
    if (distanceToPlayer < 700){
      this.pilot.accelerateToSpeed(LUDICROUS);
      // fire any guns picked up along the way >:D
      this.pilot.fire();
    } else {
      this.pilot.accelerateToSpeed(100);
    }
  }
  
    explode(){
    super.explode();
    if (Math.random() > 0.8){
      var newSensor = new Sensor(this.game, this.x, this.y);
      this.game.world.addChild(newSensor);
    }
  }
}
