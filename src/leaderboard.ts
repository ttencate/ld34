class LeaderboardEntry {
  public name: string;
  public score: number;
  
  constructor(name: string, score: number) {
    this.name = name;
    this.score = score;
  }
}

class Leaderboard {
  private scores: Array<LeaderboardEntry>;
  
  constructor() {
    this.scores = []
  }

  getTop10() {
    return this.scores;
  }
  
  submit(name: string, score: number): number {
    var entry = new LeaderboardEntry(name, score);
    this.scores.push(entry);
    this.scores.sort((a, b) => { return b.score - a.score });
    return this.scores.indexOf(entry);
  }
}
