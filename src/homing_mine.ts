/// <reference path="component.ts"/>
/// <reference path="bullet.ts"/>
/// <reference path="mine.ts"/>
const HOMINGMINE_SPEED = 20;

class HomingMine extends BaseMine {

  constructor(game:Phaser.Game, x:number, y:number) {
    super(game, x, y, 'homing-mine');
    this.body.damping = 0.6;
  }

  update() {
    super.update();

    if (this.owner) {
      return;
    }

    var ship = window.game.ship;
    if (ship.position.distance(this.position) < 1000) {
      var towards = Phaser.Point.subtract(ship.position, this.position).normalize().multiply(HOMINGMINE_SPEED, HOMINGMINE_SPEED);
      this.body.force.x = towards.x;
      this.body.force.y = towards.y;
    } else {
      this.body.force.x = 0;
      this.body.force.y = 0;
    }
  }

  onAttach() {
    super.onAttach();
    this.body.damping = 0;
  }
}
