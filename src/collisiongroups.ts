/// <reference path="../lib/phaser.d.ts"/>

interface CollisionGroups {
  tilemap: Phaser.Physics.P2.CollisionGroup;
  playership: Phaser.Physics.P2.CollisionGroup;
  enemyship: Phaser.Physics.P2.CollisionGroup;
  components: Phaser.Physics.P2.CollisionGroup;
  playerbullets: Phaser.Physics.P2.CollisionGroup;
  enemybullets: Phaser.Physics.P2.CollisionGroup;
}
