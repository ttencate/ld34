class Warhead extends Component {
  private frameCounter: number;

  constructor(game:Phaser.Game, x:number, y:number) {
    super(game, x, y, 'warhead');
    this.frameCounter = 0;
    this.maxHealth = 200;
    this.health = 200;
  }
	  
  update(){
    super.update();
    this.frameCounter++;
	  if (this.frameCounter == 8){
      this.frame = (<number>this.frame) + 1;
	    this.frameCounter = 0;
    }
  }
}