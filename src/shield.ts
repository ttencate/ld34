/// <reference path="component.ts"/>

const SHIELD_MASS = 0.2;

class Shield extends Component {

  private shield: Phaser.Sprite;

  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'shield');
    this.body.clearShapes();
    this.body.setCircle((this.width + this.height) / 4, 0, 0, 0);
    this.fixCollisionGroup();
    this.body.mass = SHIELD_MASS;
    this.maxHealth = 130;
    this.health = 130;
  }

  update() {
    super.update();
    if (this.shield) {
      this.shield.body.x = this.x;
      this.shield.body.y = this.y;
      if (this.playerOwned && this.health < this.maxHealth){
        this.health += 2;
      }
      this.shield.alpha = this.health / this.maxHealth;
    }
  }

  onAttach() {
    super.onAttach();
    this.shield = this.game.add.sprite(0, 0, 'shield_wall');
    this.shield.alpha = 0.3;
    (<any>this.shield).owner = this; // This is so bullets can find it.

    this.game.physics.p2.enable(this.shield);
    this.shield.body.kinematic = true;
    this.shield.body.clearShapes();
    this.shield.body.setCircle((this.shield.width + this.shield.height) / 4, 0, 0, 0);
    if (this.playerOwned || this instanceof Ship) {
      this.shield.body.setCollisionGroup(this.game.cgroups.playership);
      this.shield.body.collides(this.game.cgroups.enemybullets);
    } else {
      this.shield.body.setCollisionGroup(this.game.cgroups.enemyship);
      this.shield.body.collides(this.game.cgroups.playerbullets);
    }

    this.shield.events.onKilled.add(() => { this.explode(); });
    if (this.playerOwned){
      this.maxHealth = 600;
      this.health = 600;
    } else {
      this.maxHealth = 200;
      this.health = 200;
    }
  }

  onDetach() {
    super.onDetach();

    this.shield.pendingDestroy = true;
    this.shield = null;
  }
}
