/// <reference path="component.ts"/>
/// <reference path="bullet.ts"/>

const GUN_COOLDOWN_FRAMES = 4;
const GUN_VOLUME = 0.1;
const GUN_MASS = 0.1;

class Gun extends Component {
  private cooldown: number = 0;

  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'gun');

    this.body.setCircle(this.width / 2, 0, this.height / 2 - this.width / 2, 0);
    this.fixCollisionGroup();

    this.maxHealth = 100;
    this.health = 100;
    this.body.mass = GUN_MASS;
  }

  fire(firing: boolean) {
    if (firing) {
      if (this.cooldown <= 0) {
        this.fireBullet();
        this.cooldown = GUN_COOLDOWN_FRAMES;
      } else {
        this.cooldown--;
      }
      this.frame = (<number>this.frame) + 1;
    } else {
      this.cooldown = 0;
    }
  }

  private fireBullet() {
    var position = new Phaser.Point(0, -this.height / 3);
    position.rotate(0, 0, this.body.rotation, false);
    position.add(this.body.x, this.body.y);

    createBullet(this.game, position.x, position.y, this.angle, this.body.velocity.x, this.body.velocity.y, this.playerOwned);
    window.game.sfx.play('laser', GUN_VOLUME);
  }
}
