/// <reference path="../lib/phaser.d.ts"/>

class Background {

  private game: Phaser.Game;
  private background: Phaser.TileSprite;

  constructor(game: Phaser.Game) {
    this.game = game;

    this.background = this.game.add.tileSprite(0, 0, this.game.width + 100, this.game.height + 100, 'background');
    this.background.anchor.set(0.5, 0.5);
    this.game.world.sendToBack(this.background);
  }

  update() {
    var pos = this.game.camera.position;
    var scl = this.game.camera.scale;
    this.background.position.set(pos.x / scl.x, pos.y / scl.y);
    this.background.width = (this.game.width + 100) / scl.x;
    this.background.height = (this.game.height + 100) / scl.y;
    this.background.tilePosition.set(-pos.x / 2, -pos.y / 2);
  }
}
