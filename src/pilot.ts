/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="base_ship.ts"/>

// AI class that lets us express behaviour at a higher level.
class Pilot {
  private ship: BaseShip;

  constructor(ship: BaseShip) {
    this.ship = ship;
  }

  distanceToPlayer(): number {
    return this.ship.position.distance(window.game.ship.position);
  }

  // 0: straight towards player. 180 or -180: they're right behind us!
  angleTowardsPlayer(): number {
    return Phaser.Point.angle(window.game.ship.position, this.ship.position) / Math.PI * 180 + 90;
  }

  rotateTowardsPlayer(tolerance: number = 30) {
    this.rotateTowards(0, this.angleTowardsPlayer(), tolerance);
  }

  rotateGunTowardsPlayer(angle: number =  15) {
    this.rotateTowards(this.meanGunAngle(), this.angleTowardsPlayer(), angle);
  }

  hasWeapon() {
    this.ship.forEachComponent((c) => {
      if (c instanceof Gun || c instanceof Mine) {
        return true;
      }
    });
    return false;
  }

  fire() {
    if (window.game.ship.alive) {
      this.ship.fire(true);
    }
  }

  private meanGunAngle(): number {
    var gunAngle = 0;
    this.ship.forEachComponent((c) => {
      if (c instanceof Gun) {
        gunAngle = c.angle - this.ship.angle;
      }
    });
    return gunAngle;
  }

  private rotateTowards(localAngle: number, targetAngle: number, tolerance: number) {
    var myAngle = this.ship.angle + localAngle;
    var delta = (((myAngle - targetAngle) % 360 - 180) % 360) + 180;
    if (Math.abs(delta) > tolerance) {
      if (delta < 0) {
        this.ship.rotate(-1);
      } else {
        this.ship.rotate(1);
      }
    } else {
      this.ship.rotate(0);
    }
  }

  accelerateToSpeed(speed: number) {
    if (this.currentSpeed() < speed) {
      this.ship.accelerate(1);
    } else {
      this.ship.accelerate(0);
    }
  }

  currentSpeed(): number {
    var v = this.ship.body.velocity;
    return Math.sqrt(v.x*v.x + v.y*v.y);
  }
}
