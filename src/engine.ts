/// <reference path="component.ts"/>
import Emitter = Phaser.Particles.Arcade.Emitter;

const ENGINE_FORCE = 600;
const ENGINE_MASS = 0.5;
const tmpPoint = new Phaser.Point();

class Engine extends Component {
  private emitter: Emitter;

  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'engine');

    this.body.clearShapes();
    this.body.addPolygon({}, [
      0, 40,
      15, 51,
      30, 51,
      44, 40,
      30, 2,
      15, 2
    ]);
    this.fixCollisionGroup();
    this.body.x = x;
    this.body.y = y;

    this.emitter = new Emitter(game, 0, 0, 200);
    this.emitter.makeParticles('exhaust-particle');
    this.emitter.gravity = 0;
    this.emitter.lifespan = 500;

    this.emitter.setRotation(90, 90);
    this.emitter.setAlpha(0.8, 0.1, 500);
    this.emitter.setScale(0.8, 0.0, 1.0, 0.0, 500, Phaser.Easing.Quintic.Out);

    this.body.mass = ENGINE_MASS;//this.body.mass / 4;
    this.maxHealth = 200;
    this.health = 200;
  }

  update() {
    super.update();
    tmpPoint.set(0, 16);
    tmpPoint.rotate(0, 0, this.angle, true);
    tmpPoint.add(this.x, this.y);
    this.emitter.x = tmpPoint.x;
    this.emitter.y = tmpPoint.y;
  }
  // Applies thrust during the coming tick only.
  public thrust(enabled: boolean) {
    if (enabled) {
      var force = new Phaser.Point(0, -ENGINE_FORCE);
      force.rotate(0, 0, this.body.rotation, false);
      this.body.force.x += force.x;
      this.body.force.y += force.y;
      this.frame = 1;

      this.emitter.minParticleSpeed.setTo(-0.9 * force.x, -0.9 * force.y);
      this.emitter.maxParticleSpeed.setTo(-1.1 * force.x, -1.1 * force.y);
      this.emitter.emitParticle();
    } else {
      this.frame = 0;
    }
  }
}
