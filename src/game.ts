/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="level.ts"/>
/// <reference path="score.ts"/>
/// <reference path="leaderboard.ts"/>

interface Number {
    mod(n: number): number;
}
Number.prototype.mod = function(n) {
    return ((this%n)+n)%n;
};


class SimpleGame {

  private level: string;
  private game: Phaser.Game;
  public  ship: Ship;
  private cursors: Phaser.CursorKeys;
  private spaceBar: Phaser.Key;
  private background: Background;
  public  score: Score;
  public  multiplyer: number;
  public  maxMultiplyer: number;
  public  multiplyerDisplay: Counter;
  public  maxMultiplyerDisplay: Counter;
  public  sfx: Phaser.AudioSprite;
  private musics: Array<Phaser.Sound>;
  private currentMusic: Phaser.Sound;
  private funeralMusic: Phaser.Sound;
  public  explosion: Phaser.Particles.Arcade.Emitter;
  public  layer: Phaser.TilemapLayer;
  private leaderboard: Leaderboard;
  private destroyed: boolean;

  constructor(level: string) {
    this.level = level;
    this.game = new Phaser.Game(1024, 768, Phaser.AUTO, 'canvas', {preload: this.preload.bind(this), create: this.create.bind(this), update: this.update.bind(this), render: this.render.bind(this)});
    this.leaderboard = new Leaderboard();
  }

  preload() {
    this.game.stage.disableVisibilityChange = true;

    this.game.load.image('tileset', './img/tileset.png');
    this.game.load.tilemap('arena', './maps/arena.json', null, Phaser.Tilemap.TILED_JSON);
    this.game.load.image('ship', './img/energy-core-small.png');
    this.game.load.image('enemy-core', './img/enemy-core.png');
    this.game.load.spritesheet('engine', './img/base-thruster-small.png', 44, 52);
    this.game.load.spritesheet('gun', './img/phaser-animated.png', 30, 55);
    this.game.load.image('bullet', './img/bullet.png');
    this.game.load.image('enemy_bullet', './img/laser_bullet.png');
    this.game.load.image('fire', './img/fire.png');
    this.game.load.image('background', './img/stars2.jpg');
    this.game.load.image('exhaust-particle', './img/basic-thruster-ring.png');
    this.game.load.image('mine', './img/mine.png');
    this.game.load.image('homing-mine', './img/mine.png');
    this.game.load.image('mine-spawner', './img/mine-spawner.png');
    this.game.load.image('sensor', './img/sensor.png');
    this.game.load.image('shield', './img/shield.png');
    this.game.load.image('shield_wall', './img/shield_wall.png');
    this.game.load.spritesheet('warhead', './img/warhead.png', 64, 64);
    this.game.load.image('restart-btn', './img/restart-game.png');

    this.game.load.audiosprite('sfx', ['./sfx/gameaudio.mp3', './sfx/gameaudio.ogg', './sfx/gameaudio.m4a'], './sfx/gameaudio.json');

    this.game.load.audio('music1', './music/1.-Basis-bassloop-v1.mp3');
    this.game.load.audio('music2', './music/2.-Basis-bassloop-hihats-v1.mp3');
    this.game.load.audio('music3', './music/3.-Basis-bassloop-hihats-bassdrum-TRANSITION-v1.mp3');
    this.game.load.audio('music4', './music/3.-Basis-bassloop-hihats-bassdrum-v1.mp3');
    this.game.load.audio('music5', './music/5.-Music-stutterLead-v1.mp3');
    this.game.load.audio('music6', './music/6.-Music-stutterLead-eerieLead-TRANSITION-v1.mp3');
    this.game.load.audio('funeral', './music/Morsdood.mp3');
  }

  private playNextMusic() {
    // if (this.currentMusic.isPlaying) this.currentMusic.stop();
    if (this.destroyed) return;

    if (this.ship.health <= 0) {
      this.currentMusic = this.funeralMusic;
    } else {
      var idx = Math.floor(this.ship.getNumComponents() / 2) - 1;
      if (idx < 0) idx = 0;
      if (idx >= this.musics.length) idx = this.musics.length - 1;
      this.currentMusic = this.musics[idx];
    }
    
    this.currentMusic.play();
  }

  create() {

    this.sfx = this.game.add.audioSprite('sfx');
    this.musics = [];
    for (var i = 1; i <= 3; i++) {
      var music = this.game.add.audio('music' + i);
      music.onStop.add(this.playNextMusic, this);
      this.musics.push(music);
    }
    
    this.funeralMusic = this.game.add.audio('funeral');

    var scoreStyle = {font: '20px mono', fill: '#FFFFFF', stroke: '#000000', strokeThickness: 2, align: 'left'};
    this.score = new Score(this.game, 20, 20, "Score: ", scoreStyle);
    this.multiplyer = 0;
    var multiplyerStyle = {font: '20px mono', fill: '#FFFFFF', stroke: '#000000', strokeThickness: 2, align: 'left'};
    this.multiplyerDisplay = new Counter(this.game, 20, 50, "Parts: ", multiplyerStyle);
    this.maxMultiplyer = 0;
    this.maxMultiplyerDisplay = new Counter(this.game, 20, 80, "Max Parts: ", multiplyerStyle);

    this.explosion = this.game.add.emitter(0, 0, 90);
    this.explosion.makeParticles('fire');
    (<any>this.explosion).blendMode = PIXI.blendModes.ADD;
    this.explosion.gravity = 0;
    this.explosion.setAlpha(1.0, 0, 1500);
    this.explosion.setScale(1, 2, 1, 2, 1500, Phaser.Easing.Quintic.Out);

    this.cursors = this.game.input.keyboard.createCursorKeys();
    this.spaceBar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.game.input.keyboard.addKey(Phaser.Keyboard.M).onDown.add(this.toggleMusic, this);
    this.game.input.keyboard.addKey(Phaser.Keyboard.D).onDown.add(this.toggleDebug, this);

    var levelObjects = setUpLevel(this.game, this.level);
    this.ship = levelObjects.ship;
    this.game.camera.follow(this.ship);
    this.background = levelObjects.background;
    this.layer = levelObjects.layer;

    this.playNextMusic();
  }

  update() {
    if (this.cursors.left.isDown) {
      this.ship.rotate(1);
    } else if (this.cursors.right.isDown) {
      this.ship.rotate(-1);
    } else {
      this.ship.rotate(0);
    }

    if (this.cursors.up.isDown) {
      this.ship.accelerate(1);
    } else if (this.cursors.down.isDown) {
      this.ship.accelerate(-1);
    } else {
      this.ship.accelerate(0);
    }
    

    this.ship.fire(this.spaceBar.isDown);

    this.multiplyer = this.ship.countChildren();
    this.multiplyerDisplay.setValue(this.multiplyer);
    
    if (this.maxMultiplyer < this.multiplyer){
      this.maxMultiplyer = this.multiplyer;
      this.maxMultiplyerDisplay.setValue(this.maxMultiplyer);
    }
    
    // make sure the UI is in front
    this.score.text.bringToTop();
    this.multiplyerDisplay.text.bringToTop();
    this.maxMultiplyerDisplay.text.bringToTop();

    this.background.update();    
  }
  
  destroy() {
    this.destroyed = true;
    this.game.destroy();
  }

  render() {
    if (this.debug) {
       this.game.debug.spriteInfo(this.ship, 25, 25);
    }
  }

  private toggleMusic() {
    this.game.sound.mute = !this.game.sound.mute;
  }

  private debug: boolean = false;
  private toggleDebug() {
    this.debug = !this.debug;
    this.game.world.forEach((entity: Phaser.Sprite) => {
      if (entity.body) {
        entity.body.debug = this.debug;
      }
    }, this);
  }
  
  gameOver(){
    console.log("ship killed, game over");
    if (this.currentMusic) { this.currentMusic.stop(); }
    this.game.camera.follow(null);
    var button = this.game.add.button((this.game.camera.width / 2) - 150, (this.game.camera.height/2) - 40, 'restart-btn', window.restartGame, this, 2, 1, 0);
    button.fixedToCamera = true;
  }
  
  
  
}
