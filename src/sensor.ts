/// <reference path="component.ts"/>

const SENSOR_ZOOM = 0.75;

class Sensor extends Component {
  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, 'sensor');
    this.body.clearShapes();
    this.body.setCircle((this.width + this.height) / 4, 0, 0, 0);
    this.fixCollisionGroup();
    this.body.mass = 0.1;
  }

  onAttach() {
    if (this.playerOwned) {
      this.setZoom(this.getZoom() * SENSOR_ZOOM);
    }
  }
  
  onDetach() {
    if (this.playerOwned) {
      this.setZoom(this.getZoom() / SENSOR_ZOOM);
    }
  }

  private setZoom(zoom: number) {
    window.game.layer.resize(this.game.width / zoom, this.game.height / zoom);
    this.game.add.tween(this.game.world.scale).to({x: zoom, y: zoom}, 200, Phaser.Easing.Linear.None, true);
    this.game.add.tween(window.game.layer).to({scrollFactorX: 1/zoom, scrollFactorY: 1/zoom}, 200, Phaser.Easing.Linear.None, true);
  }

  private getZoom(): number {
    return this.game.world.scale.x;
  }
}
