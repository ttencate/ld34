/// <reference path="../lib/phaser.d.ts"/>

class Counter {

  private game: Phaser.Game;
  public text: Phaser.Text;
  public prefix: string;
  public value: number;

  constructor(game: Phaser.Game, x:number, y:number, pre:string, style: Phaser.PhaserTextStyle) {
    this.game = game;
    this.prefix = pre;
    this.value = 0;
    //new Text(game, x, y, text, style)
    this.text = this.game.add.text(x, y, this.prefix + " " + this.value, style);
    this.text.fixedToCamera = true;
    this.text.bringToTop();
  }
  
  setValue(val:number){
    if (this.value != val){
      this.value = val;
      this.updateText();
    }
  }
  
  add(val:number){
    this.value += val;
    this.updateText();
  }
  
  sub(val:number){
    this.value -= val;
    this.updateText();
  }
  
  updateText(){
    this.text.text = this.prefix + " " + this.value;
  }
  
  setStyle(style: Phaser.PhaserTextStyle){
    this.text.setStyle(style);
  }

}
