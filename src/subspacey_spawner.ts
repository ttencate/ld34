/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="./mine.ts"/>


class SubspaceySpawner {
  private game: Phaser.Game;

  constructor(game: Phaser.Game, period: number) {
    this.game = game;
    this.game.time.events.loop(period, this.spawnSubspaceys, this);
  }

  spawnSubspaceys() {
    var pos = this.game.camera.position;
    var radii = [200, 300, 400, 500, 600];
    var angles = [0, 90, 180, 270];
    var spawnsCollisionGroup = this.game.physics.p2.createCollisionGroup();
    for (var i = 0; i < radii.length; i++) {
      var r = radii[i];
      for (var j = 0; j < angles.length; j++) {
        var angle = angles[j];
        if (Math.random() > 0.7) {
          //TODO: generate random enemy from some array.
          var newSpawn = this.game.world.addChild(new Mine(this.game, pos.x + Math.cos(angle) * r, pos.y + Math.sin(angle) * r));
          // TODO: check collisions
        }
      }
    }
  }
}
