/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="counter.ts"/>

class Score extends Counter {

  addPoints(points:number){
    this.value = this.value + points;
    this.text.text = this.prefix + " " + this.value;
  }
}
