/// <reference path="../lib/phaser.d.ts"/>

class LaserBarrier extends Phaser.Sprite {
  private firing: Boolean = true;
  private cooldown: number;

  constructor(game: Phaser.Game, x: number, y: number, angle: number) {
    super(game, x, y, 'laser-barrier');
    game.physics.p2.enable(this);
    this.body.static = true;
    this.anchor.set(0.5, 0.5);
    this.body.angle = angle;
    this.scale.setTo(0.5, 0.5);
    this.firing = true;
    this.cooldown = 0;
    this.game.time.events.loop(2*Phaser.Timer.SECOND, this.fireBurst, this);
  }

  fireBurst() {
    this.game.time.events.repeat(0.1 * Phaser.Timer.SECOND, 10, this.fireBullet, this);
  }

  private fireBullet() {
    var position = new Phaser.Point(0, -this.height / 3);
    position.rotate(0, 0, this.body.rotation, false);
    position.add(this.body.x, this.body.y);

    createBullet(this.game, position.x, position.y, this.angle, 0, 0, false);
  }

}
