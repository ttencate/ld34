/// <reference path="../lib/phaser.d.ts"/>
/// <reference path="component.ts"/>
/// <reference path="engine.ts"/>
/// <reference path="base_ship.ts"/>
/// <reference path="main.ts"/>
/// <reference path="pilot.ts"/>


class EnemyShip extends BaseShip {

  private pilot: Pilot;

  constructor(game:Phaser.Game, x:number, y:number) {
    super(game, x, y, 'enemy-core');
    this.pilot = new Pilot(this);
    
  }

  constructShip() {
    var randomShape:number = (Math.random()*3) | 0;
    switch(randomShape){
    case 0:
      var leftEngine = new Engine(this.game, this.x - 45, this.y + 20);
      var rightEngine = new Engine(this.game, this.x + 45, this.y + 20);
      var gun = new Gun(this.game, this.x, this.y - 55);
      this.game.world.addChild(leftEngine);
      this.game.world.addChild(rightEngine);
      this.game.world.addChild(gun);
      this.attach(leftEngine);
      this.attach(rightEngine);
      this.attach(gun);
      break;
    case 1:
      var soloEngine = new Engine(this.game, this.x, this.y + 55);
      var gun1 = new Gun(this.game, this.x+35, this.y);
      var gun2 = new Gun(this.game, this.x-35, this.y);
      this.game.world.addChild(soloEngine);
      this.game.world.addChild(gun1);
      this.game.world.addChild(gun2);
      this.attach(soloEngine);
      soloEngine.attach(gun1);
      soloEngine.attach(gun2);
      break;
    case 2:
    default:
      var leftEngine = new Engine(this.game, this.x - 50, this.y);
      var rightEngine = new Engine(this.game, this.x + 50, this.y);
      var gun1 = new Gun(this.game, this.x+50, this.y - 45);
      var gun2 = new Gun(this.game, this.x-50, this.y - 45);
      this.game.world.addChild(leftEngine);
      this.game.world.addChild(rightEngine);
      this.game.world.addChild(gun1);
      this.game.world.addChild(gun2);
      leftEngine.attach(gun1);
      rightEngine.attach(gun2);
      break;
    }
    
    this.maxHealth = 50;
    this.health = 50;

  }

  update() {
    super.update();
    var distanceToPlayer = this.pilot.distanceToPlayer();
    if (distanceToPlayer < 400) {
      this.pilot.rotateGunTowardsPlayer();
      this.pilot.fire();
    } else {
      
      this.pilot.rotateTowardsPlayer(20);
      
      var speed:number = 150;
      if (distanceToPlayer < 500){
        speed = 300;
      } else if (distanceToPlayer < 800) {
        speed = 300;
      } 
      this.pilot.accelerateToSpeed(speed);
      
    }
  }
}
