Ludum Dare 34

# Audio

Uses https://github.com/tonistiigi/audiosprite. Upon changing sfx files:

```
brew install ffmpeg --with-theora --with-libogg --with-libvorbis
npm run-script compile-sfx
```