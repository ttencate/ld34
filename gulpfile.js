var gulp = require('gulp');
var ts = require('gulp-typescript');

var TS_FILES = 'src/**/*.ts';

var tsProject = ts.createProject({
  noImplicitAny: true,
  out: 'js/main.js',
});

gulp.task('default', ['ts']);

gulp.task('ts', function() {
  return gulp.src(TS_FILES)
    .pipe(ts(tsProject))
    .pipe(gulp.dest('.'));
});

gulp.task('watch', ['ts'], function() {
  return gulp.watch(TS_FILES, ['ts']);
});
